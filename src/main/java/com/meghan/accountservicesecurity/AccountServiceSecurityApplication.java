package com.meghan.accountservicesecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountServiceSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountServiceSecurityApplication.class, args);
	}

}
